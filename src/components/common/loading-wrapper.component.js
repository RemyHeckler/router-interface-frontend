import React from 'react';
import styled from 'styled-components';
import { Spinner } from './spinner.component';

const SpinnerWrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export class Loading extends React.PureComponent {
  render() {
    return (
      <SpinnerWrapper>
        <Spinner size={50} color="#1BA6EA" />
      </SpinnerWrapper>
    );
  }
}
