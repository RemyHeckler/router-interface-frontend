import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const TextDiv = styled.div`
  color: ${props => (props.disabled ? 'grey' : 'black')};
`;
const Span = styled.span`
  color: red
`;

const showAsterisk = (isMandatory) => {
  if (isMandatory) {
    return <Span>*</Span>;
  }
  return null;
};

export const FieldLabel = props => (
  <TextDiv disabled={props.disabled}>
    {props.value}
    {showAsterisk(props.isMandatory)}
  </TextDiv>
);

FieldLabel.propTypes = {
  disabled: PropTypes.bool,
  value: PropTypes.string,
  isMandatory: PropTypes.bool,
};
