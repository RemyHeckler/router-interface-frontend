import React from 'react';
import { Form, NestedField } from 'react-form';
import { isEqual } from 'lodash';
import PropTypes from 'prop-types';

import { Button, SecondaryButton, Spinner, IpGroup, DnsGroup, WifiNetwork,
  WifiSecurity, Options, Content, ButtonPanel, Loading, Message } from './';
import { MandatoryFieldsValidate } from '../utils';

export class NetworkSettings extends React.PureComponent {
  static propTypes = {
    loading: PropTypes.bool,
    saving: PropTypes.bool,
    error: PropTypes.bool,
    onPressSave: PropTypes.func,
    data: PropTypes.shape({
      networkSettings: PropTypes.object,
      wifiNetworks: PropTypes.array,
    }),
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.api && nextProps.data && !isEqual(this.props.data, nextProps.data)) {
      this.state.api.setAllValues(nextProps.data.networkSettings);
    }
  }

  render() {
    const { loading, saving, error, onPressSave } = this.props;
    return (
      <Form getApi={(api) => { this.setState({ api }); }}>
        {
          (formApi) => {
            if (loading) {
              return <Loading />;
            }
            return (
              <form onSubmit={formApi.submitForm}>
                <Content>
                  <NestedField field="ethernet">
                    <Options>
                      <div><h3>Ethernet Settings</h3></div>
                      <IpGroup
                        formApi={formApi}
                        disabled={formApi.values.ethernet.isAutoIp}
                        nested="ethernet"
                      />
                      <DnsGroup
                        formApi={formApi}
                        disabled={formApi.values.ethernet.isAutoDns}
                        nested="ethernet"
                      />
                    </Options>
                  </NestedField>
                  <NestedField field="wireless">
                    <Options>
                      <div><h3>Wireless Settings</h3></div>
                      <WifiNetwork formApi={formApi} wifiNetworks={this.props.data.wifiNetworks} />
                      <WifiSecurity formApi={formApi} />
                      <IpGroup
                        formApi={formApi}
                        disabled={formApi.values.wireless.isAutoIp}
                        disabledWifi={!formApi.values.wireless.isEnabled}
                        nested="wireless"
                      />
                      <DnsGroup
                        formApi={formApi}
                        disabled={formApi.values.wireless.isAutoDns}
                        disabledWifi={!formApi.values.wireless.isEnabled}
                        nested="wireless"
                      />
                    </Options>
                  </NestedField>
                  <ButtonPanel>
                    <Button
                      type="submit"
                      onClick={() => onPressSave(formApi.values)}
                      disabled={(formApi.errors || MandatoryFieldsValidate(formApi.values))}
                    >
                    Save {saving && <Spinner />} {error && 'error'}
                    </Button>
                    <SecondaryButton>Cancel</SecondaryButton>
                    {
                      MandatoryFieldsValidate(formApi.values) &&
                      <Message color="red">
                        Fields with an asterisk are mandatory
                      </Message>
                    }
                  </ButtonPanel>
                </Content>
              </form>
            );
          }
        }
      </Form>
    );
  }
}
