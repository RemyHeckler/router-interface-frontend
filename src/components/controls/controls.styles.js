import styled from 'styled-components';
import { darken } from 'polished';

export const getLabel = (ControlIndicator, Input, defaultColor) => styled.label`
  display: block;
  position: relative;
  padding-left: 30px;
  margin-bottom: 15px;
  cursor: pointer;
  color: ${props => (props.disabled ? 'grey' : 'black')};
  &:hover {
    ${ControlIndicator} {
      background: #ccc;
    }
    ${Input}:not([disabled]):checked {
      ~ ${ControlIndicator} {
        background: ${({ color = defaultColor }) => darken(0.1, color)};
      }
    }
  }
`;

export const getInput = (BaseInput, ControlIndicator, defaultColor) => styled(BaseInput)`
  position: absolute;
  z-index: -1;
  opacity: 0;
  &:focus {
    ~ ${ControlIndicator} {
      background: #ccc;
   }
  }
  &:checked {
   ~ ${ControlIndicator} {
      background: ${({ color = defaultColor }) => color};
      &:after {
        display: block;
      }
   }
  }
  &:checked:focus {
   ~ ${ControlIndicator} {
      background: ${({ color = defaultColor }) => darken(0.1, color)};
   }
  }
  &:disabled {
   ~ ${ControlIndicator} {
      background: #e6e6e6;
      opacity: 0.6;
      pointer-events: none;
      &:after {
        background: #7b7b7b;
      }
    }
  }
`;
