import React from 'react';
import styled from 'styled-components';
import { darken } from 'polished';

export const Button = styled.button`
  display: inline-block;
  cursor: pointer;
  min-width: 120px;
  background: ${({ background = '#19A8EA' }) => background};
  color: ${({ color = '#FFF' }) => color};
  padding: 10px;
  line-height: 14px;
  font-size: 14px;
  text-align: center;
  border: none;
  margin-left: 15px;
  border-radius: 30px;
  outline: 0;
  transition: all 0.5s;
  &:hover {
    background: ${({ background = '#19A8EA' }) => darken(0.05, background)};
  }
  &[disabled] {
    background: ${({ disabledBackground = '#D8D8D8' }) => disabledBackground};
    color: ${({ disabledColor = '#FFF' }) => disabledColor};
    cursor: auto;
  }
  &:focus {
  background: ${({ background = '#19A8EA' }) => darken(0.1, background)};
}
`;

export const SecondaryButton = styled(({ color = '#19A8EA', background = '#FFF', ...rest }) =>
  <Button color={color} background={background} {...rest} />)`
  border: ${({ color = '#19A8EA' }) => `2px solid ${color}`};
`;
