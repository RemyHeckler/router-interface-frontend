import React from 'react';
import * as PropTypes from 'prop-types';
import * as ReactForm from 'react-form';
import styled from 'styled-components';

import { getInput, getLabel } from './controls.styles';

const defaultColor = '#1BA6EA';

const ControlIndicator = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  min-height: 20px;
  min-width: 20px;
  background: #e6e6e6;
  transition: all 0.3s;
  &:after {
    content: '';
    position: absolute;
    display: none;
    left: 8px;
    top: 4px;
    width: 3px;
    height: 8px;
    border: solid #fff;
    border-width: 0 2px 2px 0;
    transform: rotate(45deg);
  }
`;

const Input = getInput(ReactForm.Checkbox, ControlIndicator, defaultColor);
const Label = getLabel(ControlIndicator, Input, defaultColor);

export const Checkbox = ({ label, disabled, className, ...props }) => (
  <Label disabled={disabled} className={className}>
    {label}
    <Input disabled={disabled} type="checkbox" {...props} />
    <ControlIndicator />
  </Label>
);

Checkbox.propTypes = {
  label: PropTypes.string.isRequired,
  className: PropTypes.string,
  disabled: PropTypes.bool,
};
