import React from 'react';
import * as PropTypes from 'prop-types';
import { Radio } from 'react-form';
import styled from 'styled-components';

import { getInput, getLabel } from './controls.styles';

const defaultColor = '#1BA6EA';

const ControlIndicator = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  min-height: 20px;
  min-width: 20px;
  background: #e6e6e6;
  border-radius: 50%;
  transition: all 0.3s;
  &:after {
    content: '';
    position: absolute;
    display: none;
    left: 7px;
    top: 7px;
    height: 6px;
    width: 6px;
    border-radius: 50%;
    background: #fff;
  }
`;

const Input = getInput(Radio, ControlIndicator, defaultColor);
const Label = getLabel(ControlIndicator, Input, defaultColor);

export const RadioButton = ({ label, disabled, className, ...props }) => (
  <Label disabled={disabled} className={className}>
    {label}
    <Input disabled={disabled} type="radio" {...props} />
    <ControlIndicator />
  </Label>
);

RadioButton.propTypes = {
  label: PropTypes.string.isRequired,
  className: PropTypes.string,
  disabled: PropTypes.bool,
};
