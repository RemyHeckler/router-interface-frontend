import React from 'react';
import { Field } from 'react-form';
import PropTypes from 'prop-types';

import { Input, Message } from '../';

export const TextInput = props => (
  <Field validate={props.validate} field={props.field} {...props}>
    { (fieldApi) => {
      const { onChange, onBlur, ...rest } = props;
      const { value, error, setValue, setTouched } = fieldApi;

      return (
        <div>
          <Input
            {...rest}
            value={value || ''}
            onChange={(e) => {
              setValue(e.target.value);
              if (onChange) {
                onChange(e.target.value, e);
              }
            }}
            onBlur={(e) => {
              setTouched();
              if (onBlur) {
                onBlur(e);
              }
            }}
          />
          {error && <Message color="red">{error}</Message>}
        </div>
      );
    }}
  </Field>
);

TextInput.propTypes = {
  field: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  validate: PropTypes.func,
};
