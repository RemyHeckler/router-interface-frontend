import React from 'react';
import { IpValidate } from '../../utils';
import { TextInput } from './text-input.component';

export * from './input.styles';
export * from './text-input.component';
export * from './select-input.component';
export const IpInput = props => <TextInput {...props} validate={IpValidate} />;
