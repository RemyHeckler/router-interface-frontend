import styled from 'styled-components';

export const Input = styled.input`
  width: 200px;
  height: 35px;
  mix-blend-mode: normal;
  opacity: 0.7;
  border: 1px solid grey;
  box-sizing: border-box;
  box-shadow: inset 0 2px 6px rgba(0, 0, 0, 0.028);
  border-radius: 4px;
  font-weight: 500;
  line-height: 20px;
  font-size: 16px;
  padding: 0 6px;
  margin-left: 15px;
  &:focus {
   outline: none;
  }
  &:disabled {
    background: #F0F0F0;
  }
  @media (max-width: 1043px){
    width: 120px;
  }
`;

export const Message = styled.div`
  margin-left: 15px;
  margin-top: 5px;
  font-size: smaller;
  color: ${({ color = 'red' }) => color}
`;
