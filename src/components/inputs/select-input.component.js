import React from 'react';
import styled from 'styled-components';
import { Field } from 'react-form';
import PropTypes from 'prop-types';
import ReactSelect from 'react-select';

const StyledSelect = styled(ReactSelect)`
  margin-left: 15px;
  width: 200px;
  .Select-control {
    height: 37px;
    border-color: #a7a5a5;
  }
`;

export const Select = props => (
  <Field validate={props.validate} field={props.field} {...props}>
    {(fieldApi) => {
      const { onChange, onBlur, ...rest } = props;
      const { value, setValue, setTouched } = fieldApi;

      return (
        <div>
          <StyledSelect
            {...rest}
            name="form-field-name"
            value={value || ''}
            onChange={(option) => {
              const val = option ? option.value : null;
              setValue(val);
              if (onChange) {
                onChange(val);
              }
            }}
            onBlur={(e) => {
              setTouched();
              if (onBlur) {
                onBlur(e);
              }
            }}
          />
        </div>
      );
    }}
  </Field>
);

Select.propTypes = {
  field: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  validate: PropTypes.func,
};
