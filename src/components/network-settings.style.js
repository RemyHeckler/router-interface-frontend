import styled from 'styled-components';
import refresh from '../images/refresh_icon.svg';

export const Options = styled.div`
  flex: 1;
  min-width: 500px;
  border: 1px solid grey;
  padding: 0 10px;
  margin-left: -1px;
  @media (max-width: 800px){
    min-width: calc(100% - 21px);
  }
`;

export const Row = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin: 10px 15px 10px 0;
`;

export const Content = styled.div`
  width: 1200px;
  max-width: calc(100vw - 20px);
  padding: 20px 10px;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  @media (max-width: 800px){
    width: calc(100vw - 20px);;
  }
`;
export const ButtonPanel = styled.div`
  width: 100%;
  padding: 10px;
  margin-left: -1px;
  display: flex;
  border-bottom: 1px solid grey;
  border-left: 1px solid grey;
  border-right: 1px solid grey;
  flex-wrap: wrap;
`;
export const Refresh = styled.div`
  background-image: url(${refresh});
  border-radius: 12.5px;
  width: 25px;
  height: 25px;
  border: 1px solid grey;
  margin-left: 10px;
  opacity: ${p => (p.disabled ? 0.5 : 1)}
`;
