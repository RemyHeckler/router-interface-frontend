import React from 'react';
import PropTypes from 'prop-types';

import { Checkbox, FieldLabel, Row, TextInput } from '../';

export class WifiSecurity extends React.PureComponent {
  static propTypes = {
    formApi: PropTypes.shape({
      setValue: PropTypes.func,
      values: PropTypes.shape({
        wireless: PropTypes.shape({
          isSecured: PropTypes.bool,
          isEnabled: PropTypes.bool,
        }),
      }),
    }),
  };

  handleClickEnable = () => {
    const { setValue } = this.props.formApi;
    const { isSecured } = this.props.formApi.values.wireless;
    if (isSecured) {
      setValue('wireless.securityKey', null);
    }
  };

  render() {
    const disabledWifi = !this.props.formApi.values.wireless.isEnabled;
    const disabled = disabledWifi || !this.props.formApi.values.wireless.isSecured;
    return (
      <div>
        <Checkbox
          field="isSecured"
          onClick={this.handleClickEnable}
          disabled={disabledWifi}
          label="Enabled Wireless Security"
        />
        <Row>
          <FieldLabel disabled={disabled} value="Security Key: " isMandatory />
          <TextInput field="securityKey" disabled={disabled} />
        </Row>
      </div>
    );
  }
}
