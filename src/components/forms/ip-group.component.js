import React from 'react';
import PropTypes from 'prop-types';
import { RadioGroup } from 'react-form';

import { IpInput, FieldLabel, RadioButton, Row } from '../';

export class IpGroup extends React.PureComponent {
  static propTypes = {
    formApi: PropTypes.shape({
      setValue: PropTypes.func,
      values: PropTypes.shape({
        wireless: PropTypes.shape({
          isEnabled: PropTypes.bool,
        }),
        nested: PropTypes.shape({
          isAutoIp: PropTypes.bool,
        }),
      }),
    }),
    nested: PropTypes.string,
    disabled: PropTypes.bool,
    disabledWifi: PropTypes.bool,
  };

  onChangeRadio = () => {
    const { setValue } = this.props.formApi;
    const { nested } = this.props;
    const { isAutoIp } = this.props.formApi.values[nested];
    if (!isAutoIp) {
      setValue(`${nested}.ip`, null);
      setValue(`${nested}.netMask`, null);
      setValue(`${nested}.gateWay`, null);
    }
    if (isAutoIp) {
      setValue(`${nested}.ip`, '192.168.1.0');
      setValue(`${nested}.netMask`, '255.255.255.0');
    }
  };

  render() {
    const { disabled, disabledWifi } = this.props;
    return (
      <div>
        <RadioGroup field="isAutoIp" onChange={this.onChangeRadio} {...this.props}>
          <RadioButton value disabled={disabledWifi} label="Obtain an Ip address automatically (DHCP/BootP)" />
          <RadioButton value={false} disabled={disabledWifi} label="Use the following IP address" />
        </RadioGroup>
        <Row>
          <FieldLabel disabled={disabled} value="IP: " isMandatory />
          <IpInput field="ip" disabled={disabled} />
        </Row>
        <Row>
          <FieldLabel disabled={disabled} value="Subnet Mask: " isMandatory />
          <IpInput field="netMask" disabled={disabled} />
        </Row>
        <Row>
          <FieldLabel disabled={disabled} value="Default Gateway: " />
          <IpInput field="gateWay" disabled={disabled} />
        </Row>
      </div>
    );
  }
}
