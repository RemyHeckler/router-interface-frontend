import React from 'react';
import PropTypes from 'prop-types';
import { RadioGroup } from 'react-form';

import { IpInput, FieldLabel, RadioButton, Row } from '../';

export class DnsGroup extends React.PureComponent {
  static propTypes = {
    formApi: PropTypes.shape({
      setValue: PropTypes.func,
      values: PropTypes.shape({
        wireless: PropTypes.shape({
          isEnabled: PropTypes.bool,
          isAutoDns: PropTypes.bool,
        }),
        nested: PropTypes.shape({
          isAutoDns: PropTypes.bool,
        }),
      }),
    }),
    nested: PropTypes.string,
    disabled: PropTypes.bool,
    disabledWifi: PropTypes.bool,
  };

  onChangeRadio = () => {
    const { setValue } = this.props.formApi;
    const { nested } = this.props;
    const { isAutoDns } = this.props.formApi.values[nested];
    if (!isAutoDns) {
      setValue(`${nested}.preferredDns`, null);
      setValue(`${nested}.alternativeDns`, null);
    }
    if (isAutoDns) {
      setValue(`${nested}.preferredDns`, '192.168.1.255');
    }
  };

  render() {
    const { disabled, disabledWifi } = this.props;
    return (
      <div>
        <RadioGroup field="isAutoDns" onChange={this.onChangeRadio} {...this.props}>
          <RadioButton value disabled={disabledWifi} label="Obtain DNS address automatically" />
          <RadioButton value={false} disabled={disabledWifi} label="Use the following DS server address" />
        </RadioGroup>
        <Row>
          <FieldLabel disabled={disabled} value="Preferred DNS server: " isMandatory />
          <IpInput field="preferredDns" disabled={disabled} />
        </Row>
        <Row>
          <FieldLabel disabled={disabled} value="Alternative DNS server: " />
          <IpInput field="alternativeDns" disabled={disabled} />
        </Row>
      </div>
    );
  }
}
