import React from 'react';
import PropTypes from 'prop-types';
import { orderBy } from 'lodash';

import { Checkbox, FieldLabel, Row, Select, Refresh } from '../';

export class WifiNetwork extends React.PureComponent {
  static propTypes = {
    formApi: PropTypes.shape({
      setValue: PropTypes.func,
      values: PropTypes.shape({
        wireless: PropTypes.shape({
          isEnabled: PropTypes.bool,
        }),
      }),
    }),
    wifiNetworks: PropTypes.array,
  };

  handleClickEnable = () => {
    const { setValue } = this.props.formApi;
    const { isEnabled } = this.props.formApi.values.wireless;
    if (isEnabled) {
      setValue('wireless', {
        isEnabled: false,
        networkName: null,
        isSecured: false,
        securityKey: null,
        isAutoIp: true,
        ip: null,
        netMask: null,
        gateWay: null,
        isAutoDns: true,
        preferredDns: null,
        alternativeDns: null,
      });
    }
  };

  render() {
    const disabled = !this.props.formApi.values.wireless.isEnabled;
    const { wifiNetworks } = this.props;
    const sorted = orderBy(wifiNetworks, ['favorite', 'strength'], ['desc', 'desc']);
    const options = sorted.map(item => ({ label: item.name, value: item.name }));
    return (
      <div>
        <Checkbox field="isEnabled" onClick={this.handleClickEnable} label="Enable wifi" />
        <Row>
          <FieldLabel disabled={disabled} value="Wireless Network Name: " isMandatory />
          <Select field="networkName" options={options} disabled={disabled} />
          <Refresh disabled={disabled} />
        </Row>
      </div>
    );
  }
}
