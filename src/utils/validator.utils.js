const IpPattern = /^(?:(?:2[0-4]\d|25[0-5]|1\d{2}|[1-9]?\d)\.){3}(?:2[0-4]\d|25[0-5]|1\d{2}|[1-9]?\d)$/;

const ethernetValidator = (data) => {
  const { isAutoIp, ip, netMask, isAutoDns, preferredDns } = data;
  if (!isAutoIp && !ip) {
    return true;
  }
  if (!isAutoIp && !netMask) {
    return true;
  }
  if (!isAutoDns && !preferredDns) {
    return true;
  }
  return false;
};

const wirelessValidator = (data) => {
  const { isEnabled, networkName, isSecured, securityKey } = data;
  if (isEnabled && !networkName) {
    return true;
  }
  if (isEnabled && isSecured && !securityKey) {
    return true;
  }
  return isEnabled && ethernetValidator(data);
};

export const IpValidate = value => ({
  error: value && !IpPattern.test(value) ? 'Input must contain a correct IP' : null,
});

export const MandatoryFieldsValidate = data => ethernetValidator(data.ethernet) || wirelessValidator(data.wireless);
