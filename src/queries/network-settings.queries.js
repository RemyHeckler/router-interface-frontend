import gql from 'graphql-tag';

export const networkSettingsQuery = gql`{
  wifiNetworks {
    favorite
    name
    strength
    security
  }
  networkSettings {
    id
    ethernet {
      isAutoIp
      ip
      netMask
      gateWay
      isAutoDns
      preferredDns
      alternativeDns
    }
    wireless {
      isEnabled
      networkName
      isSecured
      securityKey
      isAutoIp
      ip
      netMask
      gateWay
      isAutoDns
      preferredDns
      alternativeDns
    }
  }
}`;

export const saveNetworkSettings = gql`
mutation ($settings: InputNetworkSettings) {
  saveNetworkSettings(settings: $settings) {
    id
    ethernet {
      isAutoIp
      ip
      netMask
      gateWay
      isAutoDns
      preferredDns
      alternativeDns
    }
    wireless {
      isEnabled
      networkName
      isSecured
      securityKey
      isAutoIp
      ip
      netMask
      gateWay
      isAutoDns
      preferredDns
      alternativeDns
    }
  }
}`;
