export class NetworkSettingsModel {
  constructor({ ethernet, wireless }) {
    this.ethernet = {
      isAutoIp: ethernet.isAutoIp,
      ip: ethernet.ip,
      netMask: ethernet.netMask,
      gateWay: ethernet.gateWay,
      isAutoDns: ethernet.isAutoDns,
      preferredDns: ethernet.preferredDns,
      alternativeDns: ethernet.alternativeDns,
    };
    this.wireless = {
      isEnabled: wireless.isEnabled,
      networkName: wireless.networkName,
      isSecured: wireless.isSecured,
      securityKey: wireless.securityKey,
      isAutoIp: wireless.isAutoIp,
      ip: wireless.ip,
      netMask: wireless.netMask,
      gateWay: wireless.gateWay,
      isAutoDns: wireless.isAutoDns,
      preferredDns: wireless.preferredDns,
      alternativeDns: wireless.alternativeDns,
    };
  }
}

