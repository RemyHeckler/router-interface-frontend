import React from 'react';
import { Query, Mutation } from 'react-apollo';
import { get } from 'lodash';
import styled from 'styled-components';

import { networkSettingsQuery, saveNetworkSettings } from './queries';
import { NetworkSettingsModel } from './models';
import { NetworkSettings } from './components';

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 100vh;
`;

class App extends React.PureComponent {
  onSave = (save, data, state) => {
    const ethernet = get(data, 'networkSettings.ethernet', {});
    const wireless = get(data, 'networkSettings.wireless', {});
    const settings = new NetworkSettingsModel({
      ethernet: { ...ethernet, ...state.ethernet },
      wireless: { ...wireless, ...state.wireless },
    });
    save({ variables: { settings } });
  };

  render() {
    return (
      <div className="App">
        <Query query={networkSettingsQuery}>
          {({ loading, data }) =>
            (
              <Mutation mutation={saveNetworkSettings}>
                {(save, { loading: saving, error }) => (
                  <Wrapper>
                    <NetworkSettings
                      loading={loading}
                      data={data}
                      saving={saving}
                      error={error}
                      onPressSave={state => this.onSave(save, data, state)}
                    />
                  </Wrapper>
                )}
              </Mutation>
            )
          }
        </Query>
      </div>
    );
  }
}

export default App;
