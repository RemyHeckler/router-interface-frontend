import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import ApolloClient from 'apollo-boost';
import 'react-select/dist/react-select.css';

import './index.css';
import App from './app';

const client = new ApolloClient({ uri: 'https://t4yphlxdj5.execute-api.eu-central-1.amazonaws.com/prod/' });

ReactDOM.render(
  (
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  ), document.getElementById('root'),
);
